

Dalton Developer's Guide
========================

.. toctree::
   :maxdepth: 2

   doc/quick_start.rst
   doc/old-clones.rst
   doc/branches.rst
   doc/faq.rst
   doc/submodules.rst
   doc/coding_standards.rst
   doc/tests.rst
   doc/bugfixes.rst
   doc/contributing.rst
