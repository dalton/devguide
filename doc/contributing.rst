

How you can contribute to this documentation
============================================

These pages are rendered using `RST/Sphinx <http://sphinx-doc.org/rest.html>`_ and served using
`Read the Docs <https://readthedocs.org>`_.
RST is a subset of Sphinx. Sphinx is RST with some extensions.


How to modify the webpages
--------------------------

The source code for this documentation is hosted on https://gitlab.com/dalton/devguide/.
You need a `GitLab <https://gitlab.com>`_ account to modify the sources.

With a GitLab account you have two possibilities to edit the sources:

* If you are member of the dalton group, you can push
  directly to https://gitlab.com/dalton/devguide/. Once you commit and push, a web-hook
  updates the documentation on http://dalton-devguide.readthedocs.io. This typically takes less than a minute.
* You fork https://gitlab.com/dalton/devguide/ and submit your changes at some point via a merge request. This means
  that your changes are not immediately visible but become so after a team member reviews your changes
  with a mouse click thus integrating them to https://gitlab.com/dalton/devguide/.

**Note that the entire documentation including the entire documentation
source code is public.**
Do not publish sensitive information and harvestable email addresses.


How to locally test changes
---------------------------

You do not have to push to see and test your changes.
You can test them locally.
For this install the necessary requirements::

  $ virtualenv venv
  $ source venv/bin/activate
  $ pip install sphinx sphinx_rtd_theme

Then build the pages with::

  $ sphinx-build . _build

Then point your browser to _build/html/index.html.
